<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}


		$this->createTable('{{%role}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string()->notNull()->unique(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%user}}', [
			'id' => $this->primaryKey(),
			'username' => $this->string()->notNull()->unique(),
			'role_id' => $this->integer()->notNull(),
			'auth_key' => $this->string(32)->notNull(),
			'password_hash' => $this->string()->notNull(),
			'password_reset_token' => $this->string()->unique(),
			'email' => $this->string()->notNull()->unique(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%staff_profile}}', [
			'id' => $this->primaryKey(),
			'user_id' => $this->integer(),
			'name' => $this->string()->notNull(),
			'nickname' => $this->string(20)->notNull(),
			'staff_no' => $this->string(20)->notNull(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%department}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%position}}', [
			'id' => $this->primaryKey(),
			'department_id' => $this->integer(),
			'name' => $this->string()->notNull(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%hod}}', [
			'id' => $this->primaryKey(),
			'staff_id' => $this->integer()->notNull(),
			'department_id' => $this->integer()->notNull(),
			'validity_start' => $this->date(),
			'validity_end' => $this->date(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%account}}', [
			'id' => $this->primaryKey(),
			'staff_id' => $this->integer(),
			'bank_branch' => $this->string(),
			'bank_name' => $this->string()->notNull(),
			'account_no' => $this->string()->notNull(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%leave_type}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%user_leave_setting}}', [
			'id' => $this->primaryKey(),
			'staff_id' => $this->integer(),
			'additional' => $this->float(),
			'expired' => $this->date(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%leave_setting}}', [
			'id' => $this->primaryKey(),
			'position_id' => $this->integer(),
			'grade_id' => $this->integer(),
			'appointed_leave' => $this->float(),
			'additional' => $this->float(),
			'expired' => $this->date(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%leave_request}}', [
			'id' => $this->primaryKey(),
			'staff_id' => $this->integer(),
			'leave_type' => $this->integer(),
			'date_from' => $this->datetime(),
			'date_to' => $this->datetime(),
			'request_at' => $this->datetime(),
			'approved' => $this->double(),
			'approved_at' => $this->datetime(),
			'approved_by' => $this->integer(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%employment_type}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%employment}}', [
			'id' => $this->primaryKey(),
			'staff_id' => $this->integer(),
			'employment_type' => $this->integer(),
			'position_id' => $this->integer(),
			'date_from' => $this->datetime(),
			'date_to' => $this->datetime(),
			'request_at' => $this->datetime(),
			'approved' => $this->double(),
			'approved_at' => $this->datetime(),
			'approved_by' => $this->integer(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%payroll_payment_type}}', [
			'id' => $this->primaryKey(),
			'payroll_id' => $this->integer(),
			'payment_type' => $this->integer(),
			'valid_from' => $this->datetime(),
			'valid_to' => $this->datetime(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%payroll}}', [
			'id' => $this->primaryKey(),
			'employment_id' => $this->integer(),
			'payment_type' => $this->integer(),
			'valid_from' => $this->datetime(),
			'valid_to' => $this->datetime(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%payroll_base}}', [
			'id' => $this->primaryKey(),
			'employment_id' => $this->integer(),
			'valid_from' => $this->datetime(),
			'valid_to' => $this->datetime(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%payroll_deduction}}', [
			'id' => $this->primaryKey(),
			'employment_id' => $this->integer(),
			'valid_from' => $this->datetime(),
			'valid_to' => $this->datetime(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);

		$this->insertData();
		$this->addForeignKey('fk1','{{%user}}','role_id','{{%role}}','id');
		$this->addForeignKey('fk2','{{%employment}}','position_id','{{%position}}','id');
		$this->addForeignKey('fk3','{{%hod}}','department_id','{{%department}}','id');
		$this->addForeignKey('fk4','{{%hod}}','staff_id','{{%staff_profile}}','id');
		$this->addForeignKey('fk5','{{%account}}','staff_id','{{%staff_profile}}','id');
		$this->addForeignKey('fk6','{{%user_leave_setting}}','staff_id','{{%staff_profile}}','id');
		$this->addForeignKey('fk7','{{%leave_setting}}','position_id','{{%position}}','id');
		$this->addForeignKey('fk8','{{%leave_request}}','leave_type','{{%leave_type}}','id');
		$this->addForeignKey('fk9','{{%staff_profile}}','user_id','{{%user}}','id');
		$this->addForeignKey('fk10','{{%position}}','department_id','{{%department}}','id');
		$this->addForeignKey('fk11','{{%leave_request}}','staff_id','{{%staff_profile}}','id');
		$this->addForeignKey('fk12','{{%leave_request}}','approved_by','{{%staff_profile}}','id');
		$this->addForeignKey('fk13','{{%employment}}','staff_id','{{%staff_profile}}','id');
		$this->addForeignKey('fk14','{{%employment}}','employment_type','{{%employment_type}}','id');
		$this->addForeignKey('fk15','{{%employment}}','position_id','{{%position}}','id');
		$this->addForeignKey('fk16','{{%employment}}','approved_by','{{%staff_profile}}','id');


	}

	public function down()
	{
// drop fk
		$this->dropForeignKey('fk1','{{%user}}');
		$this->dropForeignKey('fk2','{{%employment}}');
		$this->dropForeignKey('fk3','{{%hod}}');
		$this->dropForeignKey('fk4','{{%hod}}');
		$this->dropForeignKey('fk5','{{%account}}');
		$this->dropForeignKey('fk6','{{%user_leave_setting}}');
		$this->dropForeignKey('fk7','{{%leave_setting}}');
		$this->dropForeignKey('fk8','{{%leave_request}}');
		$this->dropForeignKey('fk9','{{%staff_profile}}');
		$this->dropForeignKey('fk10','{{%position}}');
		$this->dropForeignKey('fk11','{{%leave_request}}');
		$this->dropForeignKey('fk12','{{%leave_request}}');
		$this->dropForeignKey('fk13','{{%employment}}');
		$this->dropForeignKey('fk14','{{%employment}}');
		$this->dropForeignKey('fk15','{{%employment}}');
		$this->dropForeignKey('fk16','{{%employment}}');
// drop table
		$this->dropTable('{{%role}}');
		$this->dropTable('{{%user}}');
		$this->dropTable('{{%staff_profile}}');
		$this->dropTable('{{%leave_request}}');
		$this->dropTable('{{%leave_type}}');
		$this->dropTable('{{%user_leave_setting}}');
		$this->dropTable('{{%leave_setting}}');
		$this->dropTable('{{%account}}');
		$this->dropTable('{{%hod}}');
		$this->dropTable('{{%position}}');
		$this->dropTable('{{%department}}');
		$this->dropTable('{{%employment_type}}');
		$this->dropTable('{{%employment}}');
	}
	private function insertData()
	{

		$this->insert('{{%role}}', [
			'name' => 'Administrator',
			'description' => 'Admin User',

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%user}}', [
			'username' => 'admin1',
			'role_id' => 1,
			'auth_key' => Yii::$app->security->generateRandomString(),
			'password_hash' => Yii::$app->security->generatePasswordHash('admin1'),
			'email' => 'admin1@mail.com',

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%staff_profile}}', [
			'user_id' => 1,
			'name' => 'SAIFUL ZAHRIN',
			'nickname' => 'Zahrin',
			'staff_no' => '345',

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%department}}', [
			'name' => 'HR',
			'description' => 'Human Resource',

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%position}}', [
			'department_id' => 1,
			'name' => 'Kerani',
			'description' => 'Kerani Am',

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%leave_type}}', [
			'name' => 'Cuti Tahun',
			'description' => $this->string(),

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%leave_setting}}', [
			'position_id' => 1,
			'appointed_leave' => 18,

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%employment_type}}', [
			'name' => 'TETAP',
			'description' => 'Pekerja Tetap',

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
	}

}
