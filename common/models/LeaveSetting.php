<?php

namespace common\models;

use \common\models\base\LeaveSetting as BaseLeaveSetting;

/**
 * This is the model class for table "leave_setting".
 */
class LeaveSetting extends BaseLeaveSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['position_id', 'grade_id', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['appointed_leave', 'additional'], 'number'],
            [['expired', 'deleted_at', 'created_at', 'updated_at'], 'safe']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'position_id' => 'Position ID',
            'grade_id' => 'Grade ID',
            'appointed_leave' => 'Appointed Leave',
            'additional' => 'Additional',
            'expired' => 'Expired',
            'status' => 'Status',
        ];
    }
}
