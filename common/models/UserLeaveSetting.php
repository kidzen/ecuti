<?php

namespace common\models;

use \common\models\base\UserLeaveSetting as BaseUserLeaveSetting;

/**
 * This is the model class for table "user_leave_setting".
 */
class UserLeaveSetting extends BaseUserLeaveSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['staff_id', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['additional'], 'number'],
            [['expired', 'deleted_at', 'created_at', 'updated_at'], 'safe']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'staff_id' => 'Staff ID',
            'additional' => 'Additional',
            'expired' => 'Expired',
            'status' => 'Status',
        ];
    }
}
