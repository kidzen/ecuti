<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "leave_request".
 *
 * @property integer $id
 * @property integer $staff_id
 * @property integer $leave_type
 * @property string $date_from
 * @property string $date_to
 * @property string $request_at
 * @property double $approved
 * @property string $approved_at
 * @property integer $approved_by
 * @property integer $status
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\StaffProfile $staff
 * @property \common\models\StaffProfile $approvedBy
 * @property \common\models\LeaveType $leaveType
 */
class LeaveRequest extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['staff_id', 'leave_type', 'approved_by', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['date_from', 'date_to', 'request_at', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['approved'], 'number']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'leave_request';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'staff_id' => 'Staff ID',
            'leave_type' => 'Leave Type',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'request_at' => 'Request At',
            'approved' => 'Approved',
            'approved_at' => 'Approved At',
            'approved_by' => 'Approved By',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasOne(\common\models\StaffProfile::className(), ['id' => 'staff_id'])->inverseOf('leaveRequests');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy()
    {
        return $this->hasOne(\common\models\StaffProfile::className(), ['id' => 'approved_by'])->inverseOf('leaveRequests');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeaveType()
    {
        return $this->hasOne(\common\models\LeaveType::className(), ['id' => 'leave_type'])->inverseOf('leaveRequests');
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\LeaveRequestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\LeaveRequestQuery(get_called_class());
    }
}
