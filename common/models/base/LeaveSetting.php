<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "leave_setting".
 *
 * @property integer $id
 * @property integer $position_id
 * @property integer $grade_id
 * @property double $appointed_leave
 * @property double $additional
 * @property string $expired
 * @property integer $status
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\Position $position
 */
class LeaveSetting extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position_id', 'grade_id', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['appointed_leave', 'additional'], 'number'],
            [['expired', 'deleted_at', 'created_at', 'updated_at'], 'safe']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'leave_setting';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position_id' => 'Position ID',
            'grade_id' => 'Grade ID',
            'appointed_leave' => 'Appointed Leave',
            'additional' => 'Additional',
            'expired' => 'Expired',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(\common\models\Position::className(), ['id' => 'position_id'])->inverseOf('leaveSettings');
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\LeaveSettingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\LeaveSettingQuery(get_called_class());
    }
}
