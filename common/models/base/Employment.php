<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "employment".
 *
 * @property integer $id
 * @property integer $staff_id
 * @property integer $employment_type
 * @property integer $position_id
 * @property string $date_from
 * @property string $date_to
 * @property string $request_at
 * @property double $approved
 * @property string $approved_at
 * @property integer $approved_by
 * @property integer $status
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\StaffProfile $staff
 * @property \common\models\EmploymentType $employmentType
 * @property \common\models\Position $position
 * @property \common\models\StaffProfile $approvedBy
 */
class Employment extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['staff_id', 'employment_type', 'position_id', 'approved_by', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['date_from', 'date_to', 'request_at', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['approved'], 'number']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employment';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'staff_id' => 'Staff ID',
            'employment_type' => 'Employment Type',
            'position_id' => 'Position ID',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'request_at' => 'Request At',
            'approved' => 'Approved',
            'approved_at' => 'Approved At',
            'approved_by' => 'Approved By',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasOne(\common\models\StaffProfile::className(), ['id' => 'staff_id'])->inverseOf('employments');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmploymentType()
    {
        return $this->hasOne(\common\models\EmploymentType::className(), ['id' => 'employment_type'])->inverseOf('employments');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(\common\models\Position::className(), ['id' => 'position_id'])->inverseOf('employments')->inverseOf('employments');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy()
    {
        return $this->hasOne(\common\models\StaffProfile::className(), ['id' => 'approved_by'])->inverseOf('employments');
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\EmploymentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\EmploymentQuery(get_called_class());
    }
}
