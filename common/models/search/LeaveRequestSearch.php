<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LeaveRequest;

/**
 * common\models\search\LeaveRequestSearch represents the model behind the search form about `common\models\LeaveRequest`.
 */
 class LeaveRequestSearch extends LeaveRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'staff_id', 'leave_type', 'approved_by', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['date_from', 'date_to', 'request_at', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['approved'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LeaveRequest::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'staff_id' => $this->staff_id,
            'leave_type' => $this->leave_type,
            'date_from' => $this->date_from,
            'date_to' => $this->date_to,
            'request_at' => $this->request_at,
            'approved' => $this->approved,
            'approved_at' => $this->approved_at,
            'approved_by' => $this->approved_by,
            'status' => $this->status,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }
}
