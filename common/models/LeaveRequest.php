<?php

namespace common\models;

use \common\models\base\LeaveRequest as BaseLeaveRequest;

/**
 * This is the model class for table "leave_request".
 */
class LeaveRequest extends BaseLeaveRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['staff_id', 'leave_type', 'approved_by', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['date_from', 'date_to', 'request_at', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['approved'], 'number']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'staff_id' => 'Staff ID',
            'leave_type' => 'Leave Type',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'request_at' => 'Request At',
            'approved' => 'Approved',
            'approved_at' => 'Approved At',
            'approved_by' => 'Approved By',
            'status' => 'Status',
        ];
    }
}
