<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\EmploymentType */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Employment Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employment-type-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Employment Type'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerEmployment->totalCount){
    $gridColumnEmployment = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'staff.name',
                'label' => 'Staff'
            ],
                [
                'attribute' => 'position.name',
                'label' => 'Position'
            ],
        'date_from',
        'date_to',
        'request_at',
        'approved',
        'approved_at',
        [
                'attribute' => 'approvedBy.name',
                'label' => 'Approved By'
            ],
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerEmployment,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Employment'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnEmployment
    ]);
}
?>
    </div>
</div>
