<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Position */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Position', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="position-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Position'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF', 
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'department.name',
            'label' => 'Department',
        ],
        'name',
        'description',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerEmployment->totalCount){
    $gridColumnEmployment = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'staff.name',
                'label' => 'Staff'
            ],
            [
                'attribute' => 'employmentType.name',
                'label' => 'Employment Type'
            ],
                        'date_from',
            'date_to',
            'request_at',
            'approved',
            'approved_at',
            [
                'attribute' => 'approvedBy.name',
                'label' => 'Approved By'
            ],
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerEmployment,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-employment']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Employment'),
        ],
        'columns' => $gridColumnEmployment
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerLeaveSetting->totalCount){
    $gridColumnLeaveSetting = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'grade_id',
            'appointed_leave',
            'additional',
            'expired',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerLeaveSetting,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-leave-setting']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Leave Setting'),
        ],
        'columns' => $gridColumnLeaveSetting
    ]);
}
?>
    </div>
</div>
