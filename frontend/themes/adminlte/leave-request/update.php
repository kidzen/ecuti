<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LeaveRequest */

$this->title = 'Update Leave Request: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Leave Request', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="leave-request-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
