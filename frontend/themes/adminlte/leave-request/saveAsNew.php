<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LeaveRequest */

$this->title = 'Save As New Leave Request: '. ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Leave Request', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="leave-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
