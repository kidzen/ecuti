<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Migration */

$this->title = 'Save As New Migration: '. ' ' . $model->version;
$this->params['breadcrumbs'][] = ['label' => 'Migration', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->version, 'url' => ['view', 'id' => $model->version]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="migration-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
