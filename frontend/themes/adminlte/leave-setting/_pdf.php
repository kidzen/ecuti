<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\LeaveSetting */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Leave Setting', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leave-setting-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Leave Setting'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'position.name',
                'label' => 'Position'
            ],
        'grade_id',
        'appointed_leave',
        'additional',
        'expired',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
