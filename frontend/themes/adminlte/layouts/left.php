<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <p>Jawatan</p>
                <!-- <a href="#"><i class="fa fa-circle text-success"></i> Role</a> -->
            </div>
        </div>


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Halaman Utama', 'icon' => 'fa fa-dashboard', 'url' => ['/site/index']],
                    ['label' => 'Permohonan', 'icon' => 'fa fa-dashboard', 'items' => [
                        ['label' => 'Cuti', 'icon' => 'fa fa-share', 'url' => ['/leave-request/create']],
                    ]],
                    ['label' => 'Pentadbir', 'icon' => 'fa fa-dashboard', 'items' => [
                        ['label' => 'Ketua Tadbir', 'icon' => 'fa fa-users', 'url' => ['/hod/index']],
                        ['label' => 'Modul Cuti', 'icon' => 'fa fa-envelope', 'items' => [
                            ['label' => 'Pengesahan', 'icon' => 'fa fa-users', 'url' => ['/leave-request/index']],
                            ['label' => 'Jenis Cuti', 'icon' => 'fa fa-users', 'url' => ['/leave-type/index']],
                            ['label' => 'Setting', 'icon' => 'fa fa-users', 'url' => ['/leave-setting/index']],
                        ]],
                        ['label' => 'Modul Gaji', 'icon' => 'fa fa-users', 'items' => [
                            ['label' => 'Kontrak Staf', 'icon' => 'fa fa-users', 'url' => ['/employment/index']],
                            ['label' => 'Jenis kontrak', 'icon' => 'fa fa-users', 'url' => ['/employment-type/index']],
                        ]],
                    ]],
                    ['label' => 'Pentadbiran Sistem', 'icon' => 'fa fa-dashboard', 'items' => [
                        ['label' => 'Profil Saya', 'icon' => 'fa fa-user', 'url' => ['/staff-profile/index']],
                        ['label' => 'Penyata Gaji', 'icon' => 'fa fa-file', 'url' => ['/site/index']],
                        ['label' => 'Pengguna', 'icon' => 'fa fa-users', 'url' => ['/user/index']],
                        ['label' => 'Akses', 'icon' => 'fa fa-users', 'url' => ['/role/index']],
                        ['label' => 'Jabatan', 'icon' => 'fa fa-users', 'url' => ['/department/index']],
                        ['label' => 'Jawatan', 'icon' => 'fa fa-users', 'url' => ['/position/index']],
                    ]],
                    // ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                    // ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                    // ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    // [
                    //     'label' => 'Same tools',
                    //     'icon' => 'fa fa-share',
                    //     'url' => '#',
                    //     'items' => [
                    //         ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                    //         ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                    //         [
                    //             'label' => 'Level One',
                    //             'icon' => 'fa fa-circle-o',
                    //             'url' => '#',
                    //             'items' => [
                    //                 ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                    //                 [
                    //                     'label' => 'Level Two',
                    //                     'icon' => 'fa fa-circle-o',
                    //                     'url' => '#',
                    //                     'items' => [
                    //                         ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                    //                         ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                    //                     ],
                    //                 ],
                    //             ],
                    //         ],
                    //     ],
                    // ],
                ],
            ]
        ) ?>

    </section>

</aside>
