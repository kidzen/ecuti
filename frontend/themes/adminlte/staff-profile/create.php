<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StaffProfile */

$this->title = 'Create Staff Profile';
$this->params['breadcrumbs'][] = ['label' => 'Staff Profile', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-profile-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
