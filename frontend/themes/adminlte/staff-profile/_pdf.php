<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\StaffProfile */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Staff Profile', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-profile-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Staff Profile'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'user.username',
                'label' => 'User'
            ],
        'name',
        'nickname',
        'staff_no',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAccount->totalCount){
    $gridColumnAccount = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'bank_branch',
        'bank_name',
        'account_no',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAccount,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Account'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnAccount
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerEmployment->totalCount){
    $gridColumnEmployment = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                [
                'attribute' => 'employmentType.name',
                'label' => 'Employment Type'
            ],
        [
                'attribute' => 'position.name',
                'label' => 'Position'
            ],
        'date_from',
        'date_to',
        'request_at',
        'approved',
        'approved_at',
                'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerEmployment,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Employment'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnEmployment
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerHod->totalCount){
    $gridColumnHod = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                [
                'attribute' => 'department.name',
                'label' => 'Department'
            ],
        'validity_start',
        'validity_end',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerHod,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Hod'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnHod
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerLeaveRequest->totalCount){
    $gridColumnLeaveRequest = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                [
                'attribute' => 'leaveType.name',
                'label' => 'Leave Type'
            ],
        'date_from',
        'date_to',
        'request_at',
        'approved',
        'approved_at',
                'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerLeaveRequest,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Leave Request'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnLeaveRequest
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerUserLeaveSetting->totalCount){
    $gridColumnUserLeaveSetting = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'additional',
        'expired',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerUserLeaveSetting,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('User Leave Setting'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnUserLeaveSetting
    ]);
}
?>
    </div>
</div>
