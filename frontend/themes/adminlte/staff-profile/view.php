<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\StaffProfile */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Staff Profile', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-profile-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Staff Profile'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF', 
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'user.username',
            'label' => 'User',
        ],
        'name',
        'nickname',
        'staff_no',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAccount->totalCount){
    $gridColumnAccount = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'bank_branch',
            'bank_name',
            'account_no',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAccount,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-account']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Account'),
        ],
        'columns' => $gridColumnAccount
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerEmployment->totalCount){
    $gridColumnEmployment = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'employmentType.name',
                'label' => 'Employment Type'
            ],
            [
                'attribute' => 'position.name',
                'label' => 'Position'
            ],
            'date_from',
            'date_to',
            'request_at',
            'approved',
            'approved_at',
                        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerEmployment,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-employment']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Employment'),
        ],
        'columns' => $gridColumnEmployment
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerHod->totalCount){
    $gridColumnHod = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'department.name',
                'label' => 'Department'
            ],
            'validity_start',
            'validity_end',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerHod,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-hod']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Hod'),
        ],
        'columns' => $gridColumnHod
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerLeaveRequest->totalCount){
    $gridColumnLeaveRequest = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'leaveType.name',
                'label' => 'Leave Type'
            ],
            'date_from',
            'date_to',
            'request_at',
            'approved',
            'approved_at',
                        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerLeaveRequest,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-leave-request']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Leave Request'),
        ],
        'columns' => $gridColumnLeaveRequest
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerUserLeaveSetting->totalCount){
    $gridColumnUserLeaveSetting = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'additional',
            'expired',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerUserLeaveSetting,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-user-leave-setting']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('User Leave Setting'),
        ],
        'columns' => $gridColumnUserLeaveSetting
    ]);
}
?>
    </div>
</div>
