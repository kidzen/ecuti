<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LeaveType */

$this->title = 'Save As New Leave Type: '. ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Leave Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="leave-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
